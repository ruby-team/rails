## Rails 7.2.2.1 (December 10, 2024) ##

*   No changes.


## Rails 7.2.2 (October 30, 2024) ##

*   No changes.


## Rails 7.2.1.2 (October 23, 2024) ##

*   No changes.


## Rails 7.2.1.1 (October 15, 2024) ##

*   No changes.


## Rails 7.2.1 (August 22, 2024) ##

*   No changes.


## Rails 7.2.0 (August 09, 2024) ##

*   Testing guide: Added a section about the implicit database transaction that
    wraps tests by default.

    *Xavier Noria*

Please check [7-1-stable](https://github.com/rails/rails/blob/7-1-stable/guides/CHANGELOG.md) for previous changes.
